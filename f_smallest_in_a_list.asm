;smallest in a list of numbers
LXI H,80H	;first address of numbers
MVI B,10	;take list of 10 numbers
MVI C,0FFH	;initialize min number with max value FFH
LOOP: MOV A, M
CMP C		;compare
JNC NOSMALL
MOV C,A		;new smallest number
NOSMALL: NOP
INX H
DCR B
JNZ LOOP
MOV A,C
STA 8AH		;store smallest number at 8AH
HLT
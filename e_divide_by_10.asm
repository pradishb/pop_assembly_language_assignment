;divide by 10
LDA 80H
MVI B,0		;initialize quotient as 0
LOOP: CPI 10	;compare with 10
JC FINISH
SBI 10		;subtract number by 10
INR B
JMP LOOP
FINISH: NOP
STA 82H		;store remainder at 82H
MOV A,B
STA 81H		;store quotient at 81H
HLT
;sum of digits
LDA 80H
ANI 0FH		;take only last digit by masking
MOV B,A
LDA 80H
RRC
RRC
RRC
RRC		;swap digits by rotating
ANI 0FH		;take only last digit by masking
ADD B
STA 81H
HLT